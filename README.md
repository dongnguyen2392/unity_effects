#  Unity Effects

## Các package trong git này
* [FX Quest](#fx-quest)
* [Cartoon FX Remaster vR 1.2.0](#cartoon-fx-remaster)
* [Epic Toon FX v1.8](#epic-toon-fx)
* [Confetti VFX 2.5](#confetti-vfx)
## FX Quest
Assets\FX Quest\Particles\Prefabs
## Cartoon FX Remaster
Assets\JMO Assets
## Epic Toon FX v1.8
Assets\Epic Toon FX\Prefabs
Assets\Epic Toon FX\Prefabs 2D
## Confetti VFX 2.5
Assets\Luke Peek\Confetti\Prefabs